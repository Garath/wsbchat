﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WSBChat.Client
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private HubConnection connection;

		public MainWindow()
		{
			InitializeComponent();

			connection = new HubConnectionBuilder()
				.WithUrl("https://localhost:44301/ChatHub")
				.Build();

			sendButton.IsEnabled = false;
		}

		private async void connectButton_Click(object sender, RoutedEventArgs e)
		{
			connection.On<string, string>("ReceiveMessage", (user, message) =>
			{
				this.Dispatcher.Invoke(() =>
				{
					var newMessage = $"{user}: {message}";
					messagesList.Items.Add(newMessage);
				});
			});

			try
			{
				await connection.StartAsync();
				messagesList.Items.Add("Connection started");
				connectButton.IsEnabled = false;
				sendButton.IsEnabled = true;
			}
			catch (Exception ex)
			{
				messagesList.Items.Add(ex.Message);
			}
		}

		private async void sendButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				await connection.InvokeAsync("SendMessage",
				userTextBox.Text, messageTextBox.Text);
			}
			catch (Exception ex)
			{
				messagesList.Items.Add(ex.Message);
			}
		}
	}
}
